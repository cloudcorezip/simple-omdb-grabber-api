# Simple OMDb API Poster Grabber

# Getting Started
1. install the dependencies with
```
$ yarn
``` 
2. copy the .env.example 
```
$ cp .env.example .env
// PS: Don't forget to adjust the .env variables with config on your system like pg_user, pg_port, etc.
```
3. create database and run the migrations
```
$ yarn run sequelize db:create
$ yarn run sequelize db:migrate
```
4. run the app
```
$ yarn run nodemon app.js
```

# API Documentation

### Without Authentication
- signup ```POST /api/auth/signin```
    Register a new user
    - Payload Example
    ```
    {
        'username': 'foo',
        'password': 'oof'
    }
    ```
    
    - Success Response
    ```
    {
       message: "User Created" 
    } 200 OK
    ```
    
- signin ```POST /api/auth/signup```
    Login and Retrive Token
    - Payload Example
    ```
    {
        'username': 'foo'
        'password': 'oof'
    }
    ```
    - Success Response
    ```
    {
        "id": 1,
        "username": "tes123",
        "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNjI5OTkxMjg3fQ.q5afIw_e47-HU2OXq0q3CIVewa986F04kJQHoHTWOfs",
        "token_type": "bearer"
    } // 200 OK
    ```
    
    - Error Response
        - Wrong Password
        ```
        { 
           "message": "invalid password"
        } // 200 OK
        ```
        - Username not found
        ```
        {
            "message": "username not found"
        } // 404 Not Found
        ```
    
### With Authentication
Closed endpoints require a valid Token to be included in the header of the request. A Token can be acquired from the Login view above.
To use the token  ```Authorization: Bearer {Token}```

-   Get Movie Posters by title ```GET /api/movies/{title}?page={page}``` 
    - Example : ```host/api/movies/the_room``` (yes the underscore is a space)
        - Success Response
        ```
        {
            "data": [
                {
                "title": "O Brother, Where Art Thou?",
                "poster": "https://m.media-amazon.com/images/M/MV5BMjZkOTdmMWItOTkyNy00MDdjLTlhNTQtYzU3MzdhZjA0ZDEyXkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_SX300.jpg"
                },
                {
                "title": "Brother Bear",
                "poster": "https://m.media-amazon.com/images/M/MV5BMTIyNTAyNDYxNF5BMl5BanBnXkFtZTYwNjYwMjg2._V1_SX300.jpg"
                },
            ],
            total_results: "123"
        } 200 OK
        ``` 
-   Save Movie Title ```POST /api/my/favorite```
    Example Payload : 
    ```
    {
        title: 'some_title'
    }
    ```
    
    - Success Response
    ```
    {
        "message": "success",
        "data": {
            "title": "some_title"
    }
    ```

- Get Favorite Movie Posters ```GET /api/my/favorite?page={page}```
    - Success Response
    ```
    {
    "message": "Success",
    "data": [
        {
            "title": "La femme de chambre du Titanic",
            "poster": "https://m.media-amazon.com/images/M/MV5BMWUzYjgyNDEtNTAyMi00M2JjLTlhMzMtMDJmOGM1ZmYzNzY4XkEyXkFqcGdeQXVyMTA0MjU0Ng@@._V1_SX300.jpg"
        },
        {
            "title": "S.O.S. Titanic",
            "poster": "https://m.media-amazon.com/images/M/MV5BZDdkYjQ1YTgtYjQ0My00ZWQwLTgyZjktNDI4NzkzOTg5NTdlXkEyXkFqcGdeQXVyNjMzMDgxMzk@._V1_SX300.jpg"
        },
        ]
    }
    ```
    
### Forbidden
Forbidden endpoint(s)
- ```GET /api/movies```
