require('dotenv').config();

const express = require('express');
const bodyParser = require('body-parser');
const expressPinoLogger = require('express-pino-logger');
const cookieParser = require('cookie-parser');
const routes = require('./src/routes');
const logger = require('./config/logger');

const app = express();
const port = 6969;

const logMid = expressPinoLogger({
  logger: logger,
  level: 'http'
});

app.use(logMid);
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(routes);


app.get(('/'), (req, res ) => {
  res.send('bruh');
})

app.listen(port, () => {
  console.log(`listen on ${port}`)
})