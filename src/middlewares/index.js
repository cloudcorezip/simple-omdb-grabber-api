const verifToken = require('./auth');
const verifSignup = require('./signup')

module.exports = {
  verifToken,
  verifSignup
}