const jwt = require("jsonwebtoken");
const config = process.env.JWT_SECRET;

verifToken = (req, res, next) => {
  let authHeader = req.headers.authorization;

  if(authHeader && req.headers.authorization.split(' ')[0] === 'Bearer') {
    const token = authHeader.split(' ')[1];
    jwt.verify(token, config, (err, decoded) => {
      if(err) {
        return res.status(401).send({
          message: "Unauthorized!"
        });
      }
      req.userId = decoded.id;
      next();
    })
  } else {
    res.sendStatus(403)
  }


}

const auth = {
  verifToken: verifToken
}
module.exports = auth;