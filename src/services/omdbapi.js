const axios = require('axios');

exports.getMovies = (title, page = 1) => {
  const key = process.env.OMDB_KEY
  axios.get(`http://www.omdbapi.com/?apikey=${OMDB_KEY}&s=${title}&page=${page}`)
    .then((res) => {
      return res.data
    }).catch((err) => {
      return err;
    })
}