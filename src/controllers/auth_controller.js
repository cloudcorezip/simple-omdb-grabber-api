const db = require('../models');
const cookies = require('cookie-parser');
const config = process.env.JWT_SECRET
const User = db.User;

var jwt = require("jsonwebtoken");
var bcrypt = require("bcryptjs");


signUp = (req, res) => {
  User.create({
    username: req.body.username,
    password: bcrypt.hashSync(req.body.password, 8)
  }).then(() => {
    res.status(200).send({ message: "User Created" })
  }).catch((err) => {
    res.status(500).send({ message: err.message })
  })
}

signIn = (req, res) => {
  User.findOne({
    where: {
      username: req.body.username
    }
  }).then((user) => {
    if(!user) {
      return res.status(404).send({ message: "User Not Found" });
    }
    let passwordIsValid = bcrypt.compareSync(
      req.body.password,
      user.password
    );

    if (!passwordIsValid) {
      return res.status(200).send({
        message: "Invalid Password!"
      });
    }
    let token = jwt.sign({ id: user.id }, config);

    res.cookie('session_id', '420');

    res.status(200).send({
      id: user.id,
      username: user.username,
      token: token,
      token_type: "bearer"
    })
  })
}

module.exports = {
  signUp,
  signIn
}