const axios = require('axios');
const db = require('../models');
const jwt = require('jsonwebtoken');
const sequelize = require('sequelize');
const apikey = process.env.OMDBAPI_KEY

const FavMovie = db.Favorite_movie;

getAllMovies = (req, res) => {
  res.status(403).send({ message: 'Forbidden Access'});
}

getMovie = (req, res) => {
  let page = req.query.page ? req.query.page : 1
  const title = req.params.title.replace(/_/g, ' ');
  axios.get(`http://www.omdbapi.com/?apikey=${apikey}&s=${title}&page=${page}`, { timeout: 5000 })
    .then((response) => {
      if(response.data.Response === 'True') {
        let resArr = [];
        for(let i = 0, len = response.data.Search.length; i < len; i++) {
          let temp = {
            title: response.data.Search[i].Title,
            poster: response.data.Search[i].Poster
          }
          resArr.push(temp);
        }
        res.status(200).send({ data: resArr, total_results: response.data.totalResults });
      } else {
        res.status(404).send(response.data)
      }
    }).catch((err) => {
      res.status(408).send({
        message: 'Request Timeout',
        Error: err.message
      })
    })
}

getFavoriteMovie = (req, res) => {
  const user_id = jwt.decode(req.headers.authorization.split(' ')[1]).id;
  let page = req.query.page ? req.query.page : 1

  FavMovie.findAll({
    where: {
      user_id: user_id
    },
    attributes: [
      [sequelize.fn('DISTINCT', sequelize.col('title')), 'title']
    ]
  }).then((Fav) => {
    const arr =  Fav.map(a => a.dataValues.title);
    let tempArr = [];
    let promArr = [];
    for(i = 0; i < arr.length; i++) {
      promArr.push(
        axios.get(`http://www.omdbapi.com/?apikey=${apikey}&s=${arr[i].replace(/_/g, ' ')}&page=${page}`)
          .then((response) => {
            if(response.data.Response === 'True') {
              for(let i = 0, len = response.data.Search.length; i < len; i++) {
                let temp = {
                  title: response.data.Search[i].Title,
                  poster: response.data.Search[i].Poster
                }
                tempArr.push(temp);
              }
            }
          }).catch((err) => {
            console.log(err)
          }) 
      )}
    
      Promise.all(promArr).then(() => {
        if(promArr.length > 0) {
          res.status(200).send({
            message: 'Success',
            data: tempArr
          })
        } else {
          res.status(404).send({
            message: 'Success',
            data: tempArr 
          })
        }
      });
  }).catch((err) => {
    res.status(500).send(err.message)
  })
}

saveFavoriteMovie = (req, res) => {
  const user_id = jwt.decode(req.headers.authorization.split(' ')[1]).id;
  FavMovie.create({
    title: req.body.title,
    user_id: user_id
  }).then((FavMovie) => {
    res.status(200).send({
      message: 'success',
      data: {
        title: FavMovie.dataValues.title
      }
    })
  }).catch((err) => {
    res.status(400).send({
      message: err.message
    })
  })
}

module.exports = {
  getAllMovies,
  getMovie,
  getFavoriteMovie,
  saveFavoriteMovie
}