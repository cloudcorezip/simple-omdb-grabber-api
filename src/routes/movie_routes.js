const express = require('express');
const { verifToken } = require('../middlewares/auth');
const controller = require('../controllers/movie_controller');
const router =  express.Router();

router.use(verifToken)

router.get("/", controller.getAllMovies);
router.get("/:title", controller.getMovie);
router.get("/my/favorite", controller.getFavoriteMovie);
router.post("/my/favorite", controller.saveFavoriteMovie);

module.exports = router;