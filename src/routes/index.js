const express = require('express');
const authRoute = require('./auth_routes')
const movieRoute = require('./movie_routes');

const router = express.Router();

const default_route = [
  {
    path: '/api/auth',
    route: authRoute
  },
  {
    path: '/api/movies',
    route: movieRoute
  }
]

default_route.forEach((route) => {
  router.use(route.path, route.route)
})

module.exports = router;