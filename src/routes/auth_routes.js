const express = require('express');
const { verifSignup } = require('../middlewares');
const controller = require("../controllers/auth_controller");

const router = express.Router();

router.post("/signin", controller.signIn);
router.post("/signup", verifSignup.checkIsUsernameAlreadyExist, controller.signUp);

module.exports = router;