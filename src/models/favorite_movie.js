'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Favorite_movie extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Favorite_movie.belongsTo(models.User, {
        foreignKey: {
          name: 'user_id',
          allowNull: false,
          constraint: false
        }
      })
      // define association here
    }
  };
  Favorite_movie.init({
    title: DataTypes.STRING,
    user_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Favorite_movie',
  });
  return Favorite_movie;
};