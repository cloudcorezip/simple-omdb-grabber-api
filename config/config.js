require('dotenv').config();

const { DB_NAME, DB_PORT, DB_USERNAME, DB_PASSWORD } = process.env

module.exports = {
  "development": {
    "username": DB_USERNAME,
    "password": DB_PASSWORD,
    "database": DB_NAME,
    "host": "127.0.0.1",
    "port": DB_PORT,
    "dialect": "postgres"
  },
  "test": {
    "username": DB_USERNAME,
    "password": DB_PASSWORD,
    "database": DB_NAME,
    "host": "127.0.0.1",
    "dialect": "postgres"
  },
  "production": {
    "username": DB_USERNAME,
    "password": DB_PASSWORD,
    "database": DB_NAME,
    "host": "127.0.0.1",
    "dialect": "postgres"
  }
}
